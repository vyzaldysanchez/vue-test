import axios from "axios";

const domainURL = "https://cat-fact.herokuapp.com";

export interface Fact {
    _id: string;
    text: string;
}

export async function getRandomFacts(): Promise<Fact[]> {
    const response = await axios.get<Fact[]>(
        `${domainURL}/facts/random?amount=20`
    );

    return response.data;
}
